// Grunt的入口文件
// 用于定义一些需要Grunt自动执行的任务
// 需要导出一个函数
// 此函数接收接收一个 grunt 的形参，内部提供一些创建任务时可以用到的API
module.exports = grunt => {
    // 任务注册
    grunt.registerTask('foo', () => {
        console.log('hello grunt')
    })
    grunt.registerTask('bar', '任务描述', () => {
        console.log('hello bar')
    })
    // grunt.registerTask('default', ['bar', 'foo'])

    // 对异步任务的支持
    grunt.registerTask('async-task', function () {
        const done = this.async()
        // 模拟异步任务
        setTimeout(() => {
            console.log('async task _________')
            // done执行了才认为异步任务完成
            done()
        }, 1000)
    })

    // 标记任务失败通过return false, 一个任务失败，后续任务都会中断
    // 命令添加 --force,即使任务失败，也会继续执行任务
    grunt.registerTask('bad', () => {
        console.log('hello grunt')
        return false
    })
    grunt.registerTask('default', ['bad', 'bar', 'foo'])
    grunt.registerTask('bad-async-task', function () {
        const done = this.async()
        // 模拟异步任务
        setTimeout(() => {
            console.log('bad-async-task _________')
            // 添加异步任务标记实参
            done(false)
        }, 1000)
    })

    // 配置参数
    grunt.initConfig({
        foo: {
            bar: 123
        },
        // 多目标任务，可以让任务根据配置形成多个子任务
        build: {
            // 公共参数
            options: {
                foo: 'baz'
            },
            css: {
                foo: 'bar'
            },
            js: '2'
        }
    })
    grunt.registerMultiTask('build', function () {
        console.log(this.options())
        console.log(`target: ${this.target}, data: ${this.data}`)
    })

    // 使用插件 grunt.loadNpmTasks()
}