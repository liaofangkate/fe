// 运行在node环境下，需要按照common.js规范
const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const baseConfig = require('./webpack.base')
const { merge } = require('webpack-merge')

module.exports = merge(baseConfig, {
  // plugins: 做一些自动化工作
  plugins: [
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        'public'
      ]
    })
  ],
  // 非production模式下，tree-shaking启用，剔除未使用代码，使用前提使用ES6 module,最新的babel-loader/preset-env默认不转换ES module
  // optimization: {
  //   // 标记未使用代码
  //   usedExports: true,
  //   // 将所有模块合并到一个函数，scope-hoisting，提升运行效率，减小代码体积
  //   concatenateModules: true,
  //   // 去除未使用的代码
  //   minimize: true
  // }
}) 
