// 运行在node环境下，需要按照common.js规范
const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { compilation } = require('webpack')
const webpack = require('webpack')


// 插件必须是一个函数或者有apply方法的对象，通过在生命周期的钩子中挂载函数实现扩展
class myPlugin {
  apply (compiler) {
    console.log('---------plugin启动')
    compiler.hooks.emit.tap('myPlugin', compilation => {
      //compilatin 此次打包的上下文
      for(const name in compilation.assets) {
        if(name.endsWith('.js')){
          const contents = compilation.assets[name].source()
          const withoutComments = contents.replace(/\/\*\*+\*\//g, '')
          compilation.assets[name] = {
            source: () => withoutComments,
            // webpack要求的必须有的方法
            size: () => withoutComments.length
          }
        }
      }
    })
  }
}

module.exports = {
  // 相对路径下 ./ 不能省略
  mode: 'production',
  entry: './src/main.js',
  output: {
    // 输出文件名
    filename: 'bundle.js',
    // 输出目录，必须是绝对路径
    path: path.join(__dirname, 'output'),
    // output后面斜线不能省略，查看打包后的js文件可以看出是字符串拼接
    // 资源加载路径，默认为"",为网站根目录
    publicPath: ''
  },
  // loader: 处理不同类型的文件转为javascript代码
  module: {
    rules: [
      {
        test: /.md$/,
        // 相对路径加载
        use: ['html-loader', './markdown-loader']
      },
      // 处理es6新特性
      {
        test: /.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        },
        // 忽略安装包中的文件
        exclude: /node_modules/
      },
      // 将css转为js代码
      {
        test: /.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /.(jfif|jpg)$/,
        use: {
          loader: 'url-loader',
          options: {
            // 小于10kb,用url-loader，超过的默认file-loader
            limit: 10*1024
          }
        }
      },
      {
        test: /.html$/,
        use: {
          loader: 'html-loader',
          options: {
            attributes: {
              list: [
                {
                  tag: 'img',
                  attribute: 'src',
                  type: 'src'
                },
                {
                  tag: 'a',
                  attribute: 'href',
                  type: 'src'
                }
              ]
            }
          }
        }
      }
    ]
  },
  // plugins: 做一些自动化工作
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      // title: 'webpack-sample',
      // meta: {
      //   viewport: 'width=device-width'
      // },
      template: './src/index.html'
    }),
    // 多实例
    // new HtmlWebpackPlugin({
    //   filename: 'about.html'
    // })
    // new CopyWebpackPlugin({
    //   patterns: [
    //     'public'
    //   ]
    // }),
    new myPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  // webpack-dev-server 自动编译、刷新浏览器、提供开发服务器（打包文件在内存中，没有磁盘读写操作，提高效率）
  devServer: {
    // 开发中指定静态资源目录，开发中并不执行CopyWebpackPlugin
    contentBase: './public',
    proxy: {
      '/api': {
        target: 'https://api.github.com',
        // 不使用localhost:8080作为请求的主机名
        changeOrigin: true
      }
    },
    hot: true
    // module.hot回调函数出错也不回退到自动刷新，用于捕捉回调函数错误
    // hotOnly: true
  },
  // source Map解决了源代码与运行代码不一致，在打包后的文件最后索引source-map，调试时可直接定位到源代码而不是打包后的代码
  devtool: 'cheap-eval-source-map'
}