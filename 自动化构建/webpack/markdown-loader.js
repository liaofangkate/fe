const marked = require('marked')

// 导出一个函数，输入：加载到的资源文件的内容，返回值：输出内容必须是一段javascript代码(原因打包后的文件内要语法通过)
module.exports = source => {
  const html = marked(source)
  //方法一：返回javascript代码
  // return `export default ${JSON.stringify(html)}`

  // 方法二：交给下一个loader处理，须在use属性中配置数组 工作管道，多个loader配合完成一个任务
  return html
}