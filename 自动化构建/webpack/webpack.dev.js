// 运行在node环境下，需要按照common.js规范
const webpack = require('webpack')
const baseConfig = require('./webpack.base')
const { merge } = require('webpack-merge')

class myPlugin {
  apply (compiler) {
    console.log('---------plugin启动')
    compiler.hooks.emit.tap('myPlugin', compilation => {
      //compilatin 此次打包的上下文
      for(const name in compilation.assets) {
        if(name.endsWith('.js')){
          const contents = compilation.assets[name].source()
          const withoutComments = contents.replace(/\/\*\*+\*\//g, '')
          compilation.assets[name] = {
            source: () => withoutComments,
            // webpack要求的必须有的方法
            size: () => withoutComments.length
          }
        }
      }
    })
  }
}

module.exports = merge(baseConfig, {
  // plugins: 做一些自动化工作
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new myPlugin()
  ],
  // webpack-dev-server 自动编译、刷新浏览器、提供开发服务器（打包文件在内存中，没有磁盘读写操作，提高效率）
  devServer: {
    // 开发中指定静态资源目录，开发中并不执行CopyWebpackPlugin
    contentBase: './public',
    proxy: {
      '/api': {
        target: 'https://api.github.com',
        // 不使用localhost:8080作为请求的主机名
        changeOrigin: true
      }
    },
    hot: true
    // module.hot回调函数出错也不回退到自动刷新，用于捕捉回调函数错误
    // hotOnly: true
  }
})
