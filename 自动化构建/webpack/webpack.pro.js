// 运行在node环境下，需要按照common.js规范
const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const baseConfig = require('./webpack.base')
const { merge } = require('webpack-merge')
const OptimizieCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const TerserWebpackPlugin = require('terser-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = merge(baseConfig, {
  // 相对路径下 ./ 不能省略
  mode: 'production',
  output: {
    // 输出文件名,替换为入口名称
    filename: '[name]-[contenthash:8].js',
  },
  module: {
    // 将css转为js代码
    rules: [
      {
        test: /.css$/,
        use: [
          // 'style-loader',// 将样式通过style标签注入
          MiniCssExtractPlugin.loader, // 通过link标签的方式注入
          'css-loader'
        ]
      },
    ]
  },
  // plugins: 做一些自动化工作
  plugins: [
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        'public'
      ]
    }),
    new MiniCssExtractPlugin({
      filename: '[name]-[contenthash:8].css'
    })
  ],
  optimization: {
    splitChunks: {
      chunks: 'all'
    },
    minimizer: [
      new OptimizieCssAssetsWebpackPlugin(),
      new TerserWebpackPlugin()
    ]
  },
  // source Map解决了源代码与运行代码不一致，在打包后的文件最后索引source-map，调试时可直接定位到源代码而不是打包后的代码
  devtool: 'none'
}) 
