// 运行在node环境下，需要按照common.js规范
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'none',
  // 分包 多入口和动态导入利用es Module 动态导入方式实现按需加载
  entry: {
    main: './src/main.js',
    create: './src/create.js',
  },
  output: {
    // 输出文件名,替换为入口名称
    filename: '[name].js',
    // 输出目录，必须是绝对路径
    path: path.join(__dirname, 'output'),
    // output后面斜线不能省略，查看打包后的js文件可以看出是字符串拼接
    // 资源加载路径，默认为"",为网站根目录
    publicPath: ''
  },
  // loader: 处理不同类型的文件转为javascript代码
  module: {
    rules: [
      {
        test: /.md$/,
        // 相对路径加载
        use: ['html-loader', './markdown-loader']
      },
      // 处理es6新特性
      {
        test: /.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              // 不使用es module转换为common js,默认为false，有了tree-shaking工作的前提
              ['@babel/preset-env']
            ]
          }
        },
        // 忽略安装包中的文件
        exclude: /node_modules/
      },
      // 将css转为js代码
      {
        test: /.css$/,
        use: [
          'style-loader',// 将样式通过style标签注入
          // MiniCssExtractPlugin.loader, // 通过link标签的方式注入
          'css-loader'
        ]
      },
      {
        test: /.(jfif|jpg)$/,
        use: {
          loader: 'url-loader',
          options: {
            // 小于10kb,用url-loader，超过的默认file-loader
            limit: 10*1024
          }
        }
      },
      {
        test: /.html$/,
        use: {
          loader: 'html-loader',
          options: {
            attributes: {
              list: [
                {
                  tag: 'img',
                  attribute: 'src',
                  type: 'src'
                },
                {
                  tag: 'a',
                  attribute: 'href',
                  type: 'src'
                }
              ]
            }
          }
        }
      }
    ]
  },
  // plugins: 做一些自动化工作
  plugins: [
    new HtmlWebpackPlugin({
      // title: 'webpack-sample',
      // meta: {
      //   viewport: 'width=device-width'
      // },
      filename: 'main.html',
      template: './src/index.html',
      chunks: ['main']
    }),
    new HtmlWebpackPlugin({
      // title: 'webpack-sample',
      // meta: {
      //   viewport: 'width=device-width'
      // },
      filename: 'create.html',
      template: './src/index.html',
      chunks: ['create']
    })
  ],
  // source Map解决了源代码与运行代码不一致，在打包后的文件最后索引source-map，调试时可直接定位到源代码而不是打包后的代码
  // 最佳实践 开发：cheap-module-eval-source-map 生产：none
  devtool: 'cheap-module-eval-source-map'
}