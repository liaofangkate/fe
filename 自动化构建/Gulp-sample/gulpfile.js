// gulp的入口文件
const {series, parallel} = require("gulp")
exports.foo = done => {
    console.log('foo is working')
    done() // 标识任务完成
}

// 默认任务
exports.default = done => {
    console.log('default is working')
    done() // 标识任务完成
}

// 并行任务和串行任务
const task1 = done => {
    setTimeout(() => {
        console.log('task1 is working')
        done()
    },1000)
}
const task2 = done => {
    setTimeout(() => {
        console.log('task2 is working')
        done()
    }, 1000)
}
exports.ser = series(task1, task2)
exports.par = parallel(task1, task2)

// 异步任务
// 方法 回调函数 promise async await都可以
