// 原型链继承
function Father(firstName) {
    this.firstName = firstName
    this.ver = [1.0, 2.0]
}
function Son(lastName) {
    this.lastName = lastName
}
Son.prototype = new Father('lee')
const a = new Son('mei')
const b = new Son('lei')
console.log(a)
console.log(b)