function myNew(func, ...params) {
    const obj = {};
    obj.__proto__ = func.prototype
    const temp = func.apply(obj, params)
    if(temp && typeof temp === 'object') {
        return temp
    }else{
        return obj
    }
}
function Student(name, age) {
    this.name = name;
    this.age = age;
}
Student.prototype.say = function() {
    console.log(this)
}
const stu = myNew(Student, 'kate', 18)
console.log(stu)
stu.say() // this指向stu
