# list

## 链表：
https://leetcode-cn.com/problems/swap-nodes-in-pairs/solution/shou-hua-tu-jie-24-liang-liang-jiao-huan-lian-biao/

https://leetcode-cn.com/problems/linked-list-cycle-ii/solution/huan-xing-lian-biao-ii-by-leetcode-solution/

https://leetcode-cn.com/problems/middle-of-the-linked-list/solution/kuai-man-zhi-zhen-zhu-yao-zai-yu-diao-shi-by-liwei/

https://leetcode-cn.com/problems/remove-nth-node-from-end-of-list/

https://leetcode-cn.com/problems/merge-two-sorted-lists/

https://leetcode-cn.com/problems/reverse-linked-list/

### 补充：
https://leetcode-cn.com/problems/intersection-of-two-linked-lists/description/
https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list/description/
https://leetcode-cn.com/problems/add-two-numbers-ii/description/
https://leetcode-cn.com/problems/split-linked-list-in-parts/description/
https://leetcode-cn.com/problems/odd-even-linked-list/description/

https://leetcode-cn.com/problems/palindrome-linked-list/description/
双指针法
**总结**
链表题目常用的方法：双指针/三指针/hash表/哨兵/递归/迭代

## 数组：
https://leetcode-cn.com/problems/3sum/
https://leetcode-cn.com/problems/two-sum/
https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/
https://leetcode-cn.com/problems/move-zeroes/
https://leetcode-cn.com/problems/merge-sorted-array/
https://leetcode-cn.com/problems/majority-element/

**总结**
数组题目常用的方法：双指针/三指针/hash表/交换/赋值/摩尔投票/特殊情况处理