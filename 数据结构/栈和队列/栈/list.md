有效括号
最小栈： https://leetcode-cn.com/problems/min-stack/
232. 用栈实现队列 https://leetcode-cn.com/problems/implement-queue-using-stacks/
225. 用队列实现栈 https://leetcode-cn.com/problems/implement-stack-using-queues/

总结：辅助栈/字典/单调栈


队列Design Circular Deque（设计一个双端队列）
中文版：https://leetcode-cn.com/problems/design-circular-deque/

Sliding Window Maximum（滑动窗口最大值）
中文版：https://leetcode-cn.com/problems/sliding-window-maximum/