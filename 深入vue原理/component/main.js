import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Form from './src/Form.vue'

Vue.use(ElementUI)

new Vue({
    el: '#app',
    render: h =>  h(Form)
})