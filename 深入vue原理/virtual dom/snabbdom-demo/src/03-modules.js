import { init } from 'snabbdom/build/package/init';
import { h } from 'snabbdom/build/package/h';
import { styleModule } from 'snabbdom/src/package/modules/style';
import { eventListenersModule } from 'snabbdom/src/package/modules/eventlisteners';

// 注册模块
let patch = init([
    styleModule,
    eventListenersModule
])
let vnode = h('div', {
    style: {
        backgroundColor: 'red'
    },
    on: {
        click: handler
    }
}, [
    h('h1', 'hello snambbdom'),
    h("p", '这是一个p')
])

function handler () {
    console.log('hi')
}

let app = document.querySelector('#app')
patch(app, vnode)