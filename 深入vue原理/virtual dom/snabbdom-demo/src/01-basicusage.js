import { init } from 'snabbdom/build/package/init';
import { h } from 'snabbdom/build/package/h';

// patch函数：对比两个vnode的差异
let patch = init([])
let vnode = h('div#container.cls', 'hello World')
let app = document.querySelector('#app')
let oldVnode = patch(app, vnode)

vnode = h('div', 'hello Snabbdom')
patch(oldVnode, vnode)