import { init } from 'snabbdom/build/package/init';
import { h } from 'snabbdom/build/package/h';

// patch函数：对比两个vnode的差异
let patch = init([])
let vnode = h('div#container', [
    h('h1', 'hello snambbdom'),
    h("p", '这是一个p')
])
let app = document.querySelector('#app')
let oldVnode = patch(app, vnode)
setTimeout(() => {
    vnode = h('div#container', [
        h('h1', 'hello world'),
        h("p", 'hello key')
    ])
    patch(oldVnode, vnode)
    // 清空页面元素, 第二个参数不能传入null,通过注释节点来替换
    patch(oldVnode, h("!"))
}, 2000)