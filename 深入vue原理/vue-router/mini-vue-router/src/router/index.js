import home from '../components/home'
import detail from '../components/detail'
import error from '../components/error'
import tick from '../components/tick'

export default [
    {
        path: '/',
        name: 'default',
        component: tick
    },
    {
        path: '/home',
        name: 'home',
        component: home
    },
    {
        path: '/detail',
        name: 'detail',
        component: detail
    },
    {
        path: '*',
        name: 'error',
        component: error
    }
]