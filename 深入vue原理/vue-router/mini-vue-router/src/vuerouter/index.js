let _Vue = null
export default class VueRouter {
    constructor(options){
        this.options = options
        // 路由地址与组件的键值对
        this.routeMap = {}
        // 创建响应式对象：返回的对象可以直接用于渲染函数和计算属性内，并且会在发生变更时触发相应的更新。
        // 存储当前路径 - 这个设置为响应式为什么就能自动刷新获取对应的组件，不够清楚？
        this.data = _Vue.observable({
            current: '/'
        })
    }

    static install(Vue) {
        // 1.判断当前插件是否已经被安装
        if(VueRouter.install.installed) {
            return
        }
        VueRouter.install.installed = true
        // 2.把Vue构造函数记录到全局变量
        _Vue = Vue
        // 3.把创建Vue实例的时候传入的router对象注入到vue实例上
        // 混入
        _Vue.mixin({
            beforeCreate() {
                // 只有new Vue的时候有该数据，其他子组件的时候没有
                if(this.$options.router){
                    _Vue.prototype.$router =this.$options.router
                    this.$options.router.init()
                }
            }
        })
    }

    init() {
        this.createRouteMap()
        this.initComponents(_Vue)
        this.initEvent()
    }

    //将routes改成键值对存在routeMap内
    createRouteMap() {
        this.options.routes.forEach(route => {
            this.routeMap[route.path] = route.component
        })
    }

    //创建组件router-link router-view
    initComponents(Vue) {
        Vue.component('router-link', {
            props: {
                to: String
            },
            // template: '<a :href="to"><slot></slot></a>'
            render(h) {
                return h('a', {
                    attrs: {
                        href: this.to
                    },
                    on: {
                        // 注册点击事件，不执行
                        click: this.clickHandler
                    }
                }, [this.$slots.default])
            },
            methods: {
                clickHandler(e) {
                    // 更改浏览器地址
                    history.pushState({}, '', this.to)
                    // 加载相应的组件,this表示vue实例
                    this.$router.data.current = this.to
                    e.preventDefault()
                }
            }
        })

        // 存储给render里面用
        const self = this 
        Vue.component('router-view', {
            // h函数用于创建虚拟dom，渲染相应的组件
            render(h) {
                const component = self.routeMap[self.data.current]
                return h(component)
            }
        })
    }

    // 浏览器地址改变则加载相应的组件
    initEvent() {
        window.addEventListener('popstate', () => {
            this.data.current = window.location.pathname
        })
    }
}