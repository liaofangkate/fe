class Compiler {
    constructor(vm) {
        this.el = vm.$el
        this.vm = vm
        this.compile(this.el)
    }
    // 编译模板，处理文本节点和元素节点
    compile(el) {
        let childNodes = el.childNodes
        Array.from(childNodes).forEach(node => {
            if(this.isTextNode(node)) {
                this.compileText(node)
            }else if(this.isElementNode(node)) {
                this.compileElement(node)
            }
            // 判断是否有子节点，有的话需要递归处理
            if(node.childNodes && node.childNodes.length) {
                this.compile(node)
            }
        })
    }
    // 编译元素节点，处理指令
    compileElement(node) {
        Array.from(node.attributes).forEach(attr => {
            let attrName = attr.name
            if(this.isDirective(attrName)) {
                // 去除v-
                attrName = attrName.substr(2)
                let key = attr.value
                this.update(node, key, attrName)
            }
        })
    }
    update(node, key, attrName) {
        let updateFn =this[attrName + 'Updater']
        updateFn && updateFn.call(this, node, key, this.vm[key])
    }
    // 处理v-text
    textUpdater(node, key, value) {
        node.textContent = value
        new Watcher(this.vm, key, (newValue) => {
            node.textContent = newValue
        })
    }
    // 处理v-model
    modelUpdater(node, key, value) {
        // 用于表单元素
        node.value = value
        new Watcher(this.vm, key, (newValue) => {
            node.value = newValue
        })
        // 双向绑定，元素输入内容发生变化，需要改变data中的值
        node.addEventListener('input', () => {
            this.vm[key] = node.value
        })
    }
    // 编译元素节点，处理插值表达式
    compileText(node) {
        let reg = /\{\{(.+?)\}\}/
        let value = node.textContent
        if(reg.test(value)) {
            // 获取第一个分组的内容
            let key = RegExp.$1.trim()
            node.textContent = value.replace(reg, this.vm[key])
            // 首次渲染时创建watcher对象
            new Watcher(this.vm, key, (newValue) => {
                node.textContent = newValue
            })
        }
    }
    // 判断元素是否是指令
    isDirective(attrName) {
        return attrName.startsWith('v-')
    }
    // 判断是不是文本节点
    isTextNode(node) {
        return node.nodeType === 3
    }
    // 判断是不是元素节点
    isElementNode(node) {
        return node.nodeType === 1
    }
}