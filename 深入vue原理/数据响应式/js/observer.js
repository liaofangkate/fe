class Observer {
    constructor(data) {
        this.walk(data)
    }

    walk(data) {
        if(!data || typeof data !== 'object') {
            return
        }
        Object.keys(data).forEach(key => {
            this.defineReactive(data, key, data[key])
        })
    }

    defineReactive(obj, key, val) {
        const self = this
        // 负责收集依赖
        let dep = new Dep()
        // 如果val是对象，把val内部的属性转化为响应式
        this.walk(val)
        Object.defineProperty(obj, key, {
            enumerable: true,
            configurable: true,
            get() {
                // 收集依赖
                if(Dep.target) {
                    dep.addSub(Dep.target)
                }
                return val
            },
            set(newValue) {
                if(newValue === val) {
                    return
                }
                val = newValue
                // 属性赋值的新对象转化为响应式
                self.walk(newValue)
                // 数据变化需要发送通知
                dep.notify()
            }
        })
    }
}