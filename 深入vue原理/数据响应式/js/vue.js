class Vue {
    constructor(options) {
        // 保存选项数据
        this.$options = options || {}
        this.$data = options.data || {}
        this.$el = typeof options.el === 'string' ? document.querySelector(options.el) : options.el
        // 把data中的成员转换成getter和setter,注入vue实例
        this._proxyData(this.$data)
        // 调用observer，监听数据变化
        new Observer(this.$data)
        // 调用compiler，解析指令和插值表达式
        new Compiler(this)

    }
    // 将data代理到vue实示例
    _proxyData(data) {
        Object.keys(data).forEach(key => {
            Object.defineProperty(this, key, {
                enumerable: true,
                configurable: true,
                get() {
                    return data[key]
                },
                set(newValue) {
                    if(newValue === data[key]) {
                        return
                    }
                    data[key] = newValue
                }
            })
        })
    }
}