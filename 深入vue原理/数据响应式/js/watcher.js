class Watcher {
    constructor(vm, key, cb) {
        this.vm = vm
        this.key = key
        // 回调函数负责更新视图
        this.cb = cb
        // 把watcher对象记录到Dep类的静态属性target
        Dep.target = this
        // 触发get方法，在get方法中调用addSybs
        this.oldValue = vm[key]
        // 防止重复添加
        Dep.target = null
    }
    update() {
        let newValue = this.vm[this.key]
        if(this.oldValue === newValue) {
            return
        }
        this.cb(newValue)
        this.oldValue = newValue
    }
}