// 订阅者
class Watcher {
    update () {
        console.log('updating……')
    }
}
// 发布者
class Dep {
    constructor() {
        // 存储订阅者
        this.subs = []
    }

    addSubs(sub) {
        if(sub && sub.update) {
            this.subs.push(sub)
        }
    }

    notify() {
        this.subs.forEach(item => {
            item.update()
        })
    }
}

const watcher1 = new Watcher()
const watcher2 = new Watcher()

const dep = new Dep()
dep.addSubs(watcher1)
dep.addSubs(watcher2)
dep.notify()