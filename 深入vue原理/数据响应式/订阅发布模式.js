class EventEmitter {
    constructor() {
       this.subs = Object.create(null) 
    }
    // 订阅事件
    $on(eventType, handler) {
        this.subs[eventType] = this.subs[eventType] || []
        this.subs[eventType].push(handler)
    }
    // 发布事件
    $emit(eventType, ...params) {
        if(this.subs[eventType]) {
            this.subs[eventType].forEach(handler => {
                handler.apply(this, params)
            }) 
        }else{
            console.log(`${eventType} has not emitted yet`)
        }
    }
    // 注销事件
    $off(eventType, handler) {
        if(handler) {
            this.subs[eventType] = this.subs[eventType].filter(item => {
                return handler !== item
            })
        }else{
            delete this.subs[eventType]
        }
    }
    // 监听一个自定义事件，但是只触发一次。一旦触发之后，监听器就会被移除
    $once(eventType, handler) {
        // emit后执行相应的事件然后移除
        function myOn (...params) {
            handler.apply(this, params)
            this.$off(eventType, myOn)
        }
        // 注册包装好的事件
        this.$on(eventType, myOn)
    }
}

const e = new EventEmitter()
const f = () => {
    console.log('this is f')
}
e.$on('click', f)
e.$on('click', () => {
    console.log('click is emitted')
})
e.$on('click', () => {
    console.log('click is emitted again')
})
e.$on('change', num => {
    console.log(`change is emitted ${num}`)
})
e.$once('once', num => {
    console.log(`once is emitted ${num}`)
})
e.$emit('click')
e.$emit('change', 5)
e.$emit('once', 8)
e.$emit('once')
e.$off('click', f)
e.$emit('click')
e.$off('change')
e.$emit('change')