export default {
    state: {
      count: 0, 
    },
    mutations: {
      changeCount(state, payload) {
        state.count += payload
      }
    },
    actions: {
      changeCountAsync(context, payload) {
        setTimeout(() => {
          context.commit('changeCount', payload)
        }, 2000)
      }
    }
  }