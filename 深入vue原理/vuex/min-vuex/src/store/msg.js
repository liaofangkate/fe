export default {
    state: {
      msg: 'hello'
    },
    getters: {
      reversedMsg(state) {
        return state.msg.split('').reverse().join('')
      }
    },
    mutations: {
      changeMsg(state, payload) {
        state.msg += payload
      }
    },
    actions: {
      changeMsgAsync(context, payload) {
        setTimeout(() => {
          context.commit('changeMsg', payload)
        }, 2000)
      }
    }
  }