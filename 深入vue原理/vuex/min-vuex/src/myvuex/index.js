let _Vue = null
class Store {
    constructor(options) {
        const {
            state = {},
            getters = {},
            mutations = {},
            actions = {}
        } = options
        this.state = _Vue.observable(state)
        this.getters = Object.create(null)
        Object.keys(getters).forEach(key => {
            Object.defineProperty(this.getters, key, {
                get: () => getters[key](state)
            })
        })
        this._mutations = mutations,
        this._actions = actions
    }

    commit (type, payload) {
        this._mutations[type](this.state, payload)
    }

    dispatch (type, payload) {
        this._actions[type](this, payload)
    }
}

function install (Vue) {
    _Vue = Vue
    // 挂载$store
    _Vue.mixin({
        beforeCreate() {
            if(this.$options.store){
                // 如果是组件则不需要
                _Vue.prototype.$store = this.$options.store
            }
        }
    })
}

export default {
    Store,
    install
}