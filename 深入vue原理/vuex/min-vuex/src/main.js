import Vue from 'vue'
import App from './App.vue'
import Vuex from './myvuex'
// import count from './store/count'
// import msg from './store/msg'

Vue.config.productionTip = false
Vue.use(Vuex)

const store = new Vuex.Store({
  // modules: {
  //   count,
  //   msg
  // }
  state: {
    count: 0, 
  },
  mutations: {
    changeCount(state, payload) {
      state.count += payload
    }
  },
  actions: {
    changeCountAsync(context, payload) {
      setTimeout(() => {
        context.commit('changeCount', payload)
      }, 2000)
    }
  }
})
let vue = new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
console.log(vue)