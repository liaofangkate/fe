# Daily Report

## 2020/09/23
1.字符串方法整理            finished
2.原型链、继承blog整理      toNextDay 数据结构增加队列学习
3.js高程第二章              finished
4.数据结构：栈              finished

## 2020/09/24
1.变量类型、传值、计算       finished         
2.原型链、继承blog整理
3.js高程第三章 
4.数据结构：leetcode栈和队列题目刷题   finished 1

## 2020/09/25
1.事件                      finished
2.原型链、继承blog整理       finished
3.js高程第四章 第三章
4.数据结构：leetcode相关题目刷题-用队列实现栈 最小栈 用栈实现括号匹配  finished 3

## 2020/09/26
1.js零碎的整理                      finished
2.js高程第四章 第三章
3.数据结构：leetcode相关题目刷题-

## 2020/10/9
1.js零碎复习                finished

## 2020/10/10
1.防抖节流                  finished                
